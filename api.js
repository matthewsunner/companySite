//Fetching API data 


function pullReviews() {
    url = 'https://jsonplaceholder.typicode.com/posts';

    fetch(url)
        .then(res => res.json())
        .then(data => {
            const reviews = data;
            const reviewsMax = 15;
            for (var i = 0; i < reviewsMax; i++) {
                const title = reviews[i].title
                const body = reviews[i].body
                document.getElementById('posts').innerHTML += `
                    <div class="card" id="review-card">
                        <div class="card-header">
                            ${title}
                        </div>
                        <div class="card-body"> 
                            <p> ${body} </p>
                        </div>
                    </div>
                `;
            }

        })
}


pullReviews();