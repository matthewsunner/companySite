# Company Site 

This is a sample company website, designed by Matthew Sunner. Note: None of the content on this site reflects a real company. Any likeness to a current company is incedental and unintended. Efforts were taken to avoid likeness of pre-existing companies, however, if you notice a likeness that is in violation of any terms, please notify me right away and the likeness will be removed. 

If you have any questions, please let me know. Thanks. 


Photo Credits: 
- staffImage1 (Photo by rawpixel on Unsplash)
- staffImage2 (Photo by Jairph on Unsplash)
- staffImage3 (Photo by rawpixel on Unsplash)
- projectImage1 (Photo by The Roaming Platypus on Unsplash)
